from distutils.core import setup

with open('README.md') as f:
    readme = f.read()

setup(
    name='fortran_kernel',
    version='1.0',
    packages=['fortran_kernel',
              'fortran_kernel.tests'],
    description='Simple example kernel for Jupyter',
    long_description=readme,
    author='Jupyter Development Team',
    author_email='jupyter@googlegroups.com',
    url='https://gitlab.com/lfortran/fortran_kernel',
    install_requires=[
        'jupyter_client', 'IPython', 'ipykernel'
    ],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3',
    ],
)
