from ipykernel.kernelapp import IPKernelApp
from . import FortranKernel

IPKernelApp.launch_instance(kernel_class=FortranKernel)
