# fortran_kernel

## Installation

To install `fortran_kernel` from git:

    conda create -n ker jupyter
    conda activate ker
    pip install .
    python -m fortran_kernel.install --sys-prefix

Verify that it is installed:

    jupyter kernelspec list

## Using the Fortran kernel

**Notebook**: The *New* menu in the notebook should show an option for an Fortran notebook.

**Console frontends**: To use it with the console frontends, add `--kernel
fortran` to their command line arguments, e.g.:

    jupyter console --kernel fortran
